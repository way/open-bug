PPM Bug 缺陷管理系统
=======

项目主页： http://www.ppm123.cn

免费下载： http://ppm123.cn/pages/bug/detail.php

简介：

PPM缺陷管理系统是在见到当前免费的缺陷管理工具功能和界面都非常粗糙的情况下，旨在打造一个功能全面，界面清新的开源缺陷管理系统！

1. 工作面板 -- 快捷处理我接收的项目、缺陷，展示统计图和系统动态

2. 缺陷管理 -- 行业内领先缺陷管理流程，提高缺陷处理，管理效率

3. 项目状态 -- 按项目管理缺陷，直观统计项目下人员与缺陷的各种状态

4. 缺陷跟踪 -- 记录缺陷操作记录，流程图高亮显示操作路径与当前所处状态

5. 视图机制 -- 用户可在缺陷列表和工作面板中定制想看到的缺陷列表

6. 经验分享 -- 缺陷经验分享，跟其他研发人员分享缺陷解决的经验

7. 定制页面 -- 用户可自定义缺陷操作与查看页面的表单字段


版本：

PPM Bug v1.6 是PPM缺陷管理系统于2013年9月2号发布的第7个版本。

PPM Bug v1.6 主要修改为：

1. 样式美化，修改了大量的样式，是系统看起来更加舒服，更加专业

2. 邮件发送与配置：1).当缺陷进入某个状态时向责任人发送邮件；2).每日定时向人员发送其缺陷统计情况
 
技术体系
=======

前端：

主 -- JQuery + Bootstrap + JQueryUI(bootstrap theme) + iCheck + uploadify + FancyBox + HignCharts + SVG

附 -- JQuery自定义插件 + 下拉框插件 + Bootstrap TAB插件 + 表单校验插件

后端：

主 -- SpringMVC + Sitemesh + Hibernate(注解) + Spring(Ioc) + Spring Security + JSP2 TagDir + POI + log4j

附 -- 反射 + 自定义表单 + 实体增删改查自动处理框架

源码使用
=======

查看PPM Bug的源码并运行起来十分的简单，并且源码提供了非常详细的注释

1. 首先PPM的所有开源项目都是基于open-platform平台进行开发，请先下载部署PPM平台项目

2. 下载zip包，解压

3. Eclipse引入已存在项目，选择刚才解压的文件夹

4. 关联open-platform平台项目，右键open-bug项目Build Path，在Projects中添加open-platform平台项目

5. PPM Bug采用内存数据库derby，所以只需将解压的文件夹下的db-bug文件夹放到tomcat安装目录的bin文件夹下即可，无需安装数据库

6. 打开WebRoot/WEB-INF/jdbc.properties，修改数据库配置，使用 4 步骤的derby数据库的话将url=jdbc:derby:D:/derby/bin/db-bug改为url=jdbc:derby:db-bug

7. 部署，运行，默认用户名admin，密码1
